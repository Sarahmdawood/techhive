### Node.JS Hello World application to demonstrate CI and CD

To install dependencies:
> npm install

To run tests
> npm test

To run the application
> npm start